<?php
namespace Skener\Prd\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class CategoryTest extends \TYPO3\TestingFramework\Core\Unit\UnitTestCase
{
    /**
     * @var \Skener\Prd\Domain\Model\Category
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \Skener\Prd\Domain\Model\Category();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function getNameReturnsInitialValueForString()
    {
        self::assertSame(
            '',
            $this->subject->getName()
        );
    }

    /**
     * @test
     */
    public function setNameForStringSetsName()
    {
        $this->subject->setName('Conceived at T3CON10');

        self::assertAttributeEquals(
            'Conceived at T3CON10',
            'name',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function getProductsReturnsInitialValueFor()
    {
        $newObjectStorage = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        self::assertEquals(
            $newObjectStorage,
            $this->subject->getProducts()
        );
    }

    /**
     * @test
     */
    public function setProductsForObjectStorageContainingSetsProducts()
    {
        $product = new ();
        $objectStorageHoldingExactlyOneProducts = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
        $objectStorageHoldingExactlyOneProducts->attach($product);
        $this->subject->setProducts($objectStorageHoldingExactlyOneProducts);

        self::assertAttributeEquals(
            $objectStorageHoldingExactlyOneProducts,
            'products',
            $this->subject
        );
    }

    /**
     * @test
     */
    public function addProductToObjectStorageHoldingProducts()
    {
        $product = new ();
        $productsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['attach'])
            ->disableOriginalConstructor()
            ->getMock();

        $productsObjectStorageMock->expects(self::once())->method('attach')->with(self::equalTo($product));
        $this->inject($this->subject, 'products', $productsObjectStorageMock);

        $this->subject->addProduct($product);
    }

    /**
     * @test
     */
    public function removeProductFromObjectStorageHoldingProducts()
    {
        $product = new ();
        $productsObjectStorageMock = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->setMethods(['detach'])
            ->disableOriginalConstructor()
            ->getMock();

        $productsObjectStorageMock->expects(self::once())->method('detach')->with(self::equalTo($product));
        $this->inject($this->subject, 'products', $productsObjectStorageMock);

        $this->subject->removeProduct($product);
    }
}
