<?php
namespace Skener\Prd\Domain\Model;


/***
 *
 * This file is part of the "Product" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2020 
 *
 ***/
/**
 * Category
 */
class Category extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{

    /**
     * name
     * 
     * @var string
     */
    protected $name = '';

    /**
     * products
     * 
     * @var \TYPO3\CMS\Extbase\Persistence\ObjectStorage<>
     * @TYPO3\CMS\Extbase\Annotation\ORM\Cascade("remove")
     */
    protected $products = null;

    /**
     * __construct
     */
    public function __construct()
    {

        //Do not remove the next line: It would break the functionality
        $this->initStorageObjects();
    }

    /**
     * Initializes all ObjectStorage properties
     * Do not modify this method!
     * It will be rewritten on each save in the extension builder
     * You may modify the constructor of this class instead
     * 
     * @return void
     */
    protected function initStorageObjects()
    {
        $this->products = new \TYPO3\CMS\Extbase\Persistence\ObjectStorage();
    }

    /**
     * Returns the name
     * 
     * @return string $name
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Sets the name
     * 
     * @param string $name
     * @return void
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * Adds a
     * 
     * @param  $product
     * @return void
     */
    public function addProduct($product)
    {
        $this->products->attach($product);
    }

    /**
     * Removes a
     * 
     * @param $productToRemove The  to be removed
     * @return void
     */
    public function removeProduct($productToRemove)
    {
        $this->products->detach($productToRemove);
    }

    /**
     * Returns the products
     * 
     * @return \TYPO3\CMS\Extbase\Persistence\ObjectStorage<> $products
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Sets the products
     * 
     * @param \TYPO3\CMS\Extbase\Persistence\ObjectStorage<> $products
     * @return void
     */
    public function setProducts(\TYPO3\CMS\Extbase\Persistence\ObjectStorage $products)
    {
        $this->products = $products;
    }
}
