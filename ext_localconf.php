<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
            'Skener.Prd',
            'Prd',
            [
                'Product' => 'list, show, new, create, edit, update, delete',
                'Category' => 'list, show, new, create, edit, update, delete'
            ],
            // non-cacheable actions
            [
                'Product' => 'create, update, delete',
                'Category' => 'create, update, delete'
            ]
        );

    // wizards
    \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPageTSConfig(
        'mod {
            wizards.newContentElement.wizardItems.plugins {
                elements {
                    prd {
                        iconIdentifier = prd-plugin-prd
                        title = LLL:EXT:prd/Resources/Private/Language/locallang_db.xlf:tx_prd_prd.name
                        description = LLL:EXT:prd/Resources/Private/Language/locallang_db.xlf:tx_prd_prd.description
                        tt_content_defValues {
                            CType = list
                            list_type = prd_prd
                        }
                    }
                }
                show = *
            }
       }'
    );
		$iconRegistry = \TYPO3\CMS\Core\Utility\GeneralUtility::makeInstance(\TYPO3\CMS\Core\Imaging\IconRegistry::class);

			$iconRegistry->registerIcon(
				'prd-plugin-prd',
				\TYPO3\CMS\Core\Imaging\IconProvider\SvgIconProvider::class,
				['source' => 'EXT:prd/Resources/Public/Icons/user_plugin_prd.svg']
			);

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerTypeConverter(\Skener\Prd\Property\TypeConverter\UploadedFileReferenceConverter::class);
        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerTypeConverter(\Skener\Prd\Property\TypeConverter\ObjectStorageConverter::class);
    }
);
