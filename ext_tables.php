<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
            'Skener.Prd',
            'Prd',
            'Product'
        );

        if (TYPO3_MODE === 'BE') {

            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
                'Skener.Prd',
                'web', // Make module a submodule of 'web'
                'prd', // Submodule key
                '', // Position
                [
                    'Product' => 'list, show, new, create, edit, update, delete','Category' => 'list, show, new, create, edit, update, delete',
                ],
                [
                    'access' => 'user,group',
                    'icon'   => 'EXT:prd/Resources/Public/Icons/user_mod_prd.svg',
                    'labels' => 'LLL:EXT:prd/Resources/Private/Language/locallang_prd.xlf',
                ]
            );

        }

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('prd', 'Configuration/TypoScript', 'Product');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_prd_domain_model_product', 'EXT:prd/Resources/Private/Language/locallang_csh_tx_prd_domain_model_product.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_prd_domain_model_product');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_prd_domain_model_category', 'EXT:prd/Resources/Private/Language/locallang_csh_tx_prd_domain_model_category.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_prd_domain_model_category');

    }
);
