#
# Table structure for table 'tx_prd_domain_model_product'
#
CREATE TABLE tx_prd_domain_model_product (

	title varchar(255) DEFAULT '' NOT NULL,
	price int(11) DEFAULT '0' NOT NULL,
	image int(11) unsigned DEFAULT '0',
	category int(11) unsigned DEFAULT '0',
	tstamp int(11) unsigned DEFAULT 0 NOT NULL,
    crdate int(11) unsigned DEFAULT 0 NOT NULL,
    deleted tinyint(4) unsigned DEFAULT 0 NOT NULL
);

#
# Table structure for table 'tx_prd_domain_model_category'
#
CREATE TABLE tx_prd_domain_model_category (

	name varchar(255) DEFAULT '' NOT NULL,
	products int(11) unsigned DEFAULT '0' NOT NULL,

);
